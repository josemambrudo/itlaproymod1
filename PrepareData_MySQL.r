#Grupo 1
#Script: PrepareData.r
#Autor: Manuel Jose
#Fecha: 12-02-2018
#Objetivo: Intrucciones preparativas de carga de datos del proyecto.
#Objetivos Especificos:
#   -Desde R, descargar y descomprimir los ficheros correspondientes a los años dados.
#   -Cargar en base de datos los ficheros
#   -Retornor un dataframe con los datos cargados, años y cantidad de lineas cargadas


#Paquetes librerias necesarias
#install.packages("RMySQL",dependencies = T)

#Cargar libreria RMySQL
require(DBI)
library(RMySQL)

#Constantes de conecion a base de datos, cambiar a discreción
prop.dbname <- 'proymod1'
prop.host <-  '127.0.0.1'
prop.port <-  3306
prop.user = 'itladsgpo'
prop.password <-  '123456'

#Funcion que retornora la variable de conecion de conecion a la base de datos
openConnectionDB <- function() {
        conn <- dbConnect(MySQL(),
                         dbname = prop.dbname,
                         host = prop.host,
                         port = prop.port ,
                         user = prop.user,
                         password = prop.password)
        conn
}

closeConnectionDB <- function(conn){
        dbDisconnect(conn)
}

#dropTables esta funcion conecta con la con la base e datos y borrar las tablas indicadas en un vector.
dropTables <-function(){
  # Conectar con una instancia de postgreSQL
  conn <- openConnectionDB()

  #Recorrer el vector de nombre de tablas
  for (t in c('airports','carrier','rita'))
    dbRemoveTable(conn,t) #Borrar las tablas con nombre t

  # Desconectar de la instancia
  closeConnectionDB(conn)
}

#savedftodb, funcion que se conecta a la base de datos, y guarda los datos al df
savedftodb <- function(nameTable,df){

  # Conectar con la instancia de base de datos
  conn <- openConnectionDB()

  #Escribe la tabla, y agrega los registros, append si exite la base de datos agregar los datos
  dbWriteTable(conn, nameTable, append=TRUE , df)

  # Desconectar de la instancia
  closeConnectionDB(conn)
}

#savedftodb: Baja los datos desde la pagina: http://stat-computing.org/dataexpo/2009/; Lee y descomprime con la funcion bzfile combinada con read_csv, posterior guarda los
#datos en la tabla que se ha indicado en el primer parametro, por defecto se indica no descargar los archivos.
loadFile <- function(year,downloadFile=F){
  kURLPATH <- 'http://stat-computing.org/dataexpo/2009/'  #constante donde esta el repositorio de archivos
  ext <- '.csv.bz2'; #extension de los ficheros de anos
  file <- paste(year,ext,sep='') # pegar año, extension, separacion sin espacio y asignar a la variable file, este archivo sera el destino

  url = paste(kURLPATH,file,sep='')  # pegar constante kURLPATH, fichero o año,  sin espacio y asignar a la variable url
  if (downloadFile == T){
    print(paste(c("Descargar archivo:",file),sep=" "))
    download.file(url,file) #decarga el fichero en la url, y es destinado a file
  }

  df <- read.csv(bzfile(file),header = T, sep = ',') #lee el fichero descargado descomprime el archivo en memoria, contiene cabecera, y es separado por coma, este devolvera un dataframe
  print(paste(c("Guardar en tabla rita:",file),sep=" "))
  savedftodb('rita', df) #invoca la funcion savedftodb, y guarda en la tabla rita
}

loadFileAux <- function(aux){
  kURLPATH <- 'http://stat-computing.org/dataexpo/2009/' #constante donde esta el repositorio de archivos
  ext <- '.csv'; #extension de los archivos auxiliares.
  url <- paste(kURLPATH,aux,ext,sep='') #pegar ruta de la url, con nombre del fichero y su extension, sin separaciones, y destinar a url
  df <-  read.csv(url, sep = ",", header = TRUE  ) # leer el fichero csv directamente desde le servidor, indicar que no tiene separadores, y que poseee encabezado
  savedftodb(aux, df) #invoca la funcion savedftodb, y guarda en la tabla aux segun corresponda
}

#invoca a la funcion borrartablas si las mismas existen.
#dropTables()
#SALIDA
#[1] TRUE TRUE TRUE

#recorrer los años desde el 1987 hasta 1991, en este caso no descargar archivo porque se ha realizado previamentes, 1 recorre por fila la matriz creada, y se ejecutara en paralelo por año.
#apply(matrix(c(1988:1990)), downloadFile=F, 1, loadFile) 
#Salida
#[1] "Guardar en tabla rita:" "1987.csv.bz2"          
#[1] "Guardar en tabla rita:" "1988.csv.bz2"          
#[1] "Guardar en tabla rita:" "1989.csv.bz2"  
#[1] "Guardar en tabla rita:" "1990.csv.bz2"  
#[1] TRUE TRUE TRUE TRUE

#recorrer el vector creado airpors y carriers, ejecutar loadFileAux donde lee el csv en el servidor y guardara en nuestra bbdd.
#apply(matrix(c('airports','carriers')),1,loadFileAux)
#[1] TRUE TRUE
