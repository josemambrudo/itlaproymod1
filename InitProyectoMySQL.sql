
#Ejecutar como root inicialmente

#Crear base de datos del proyecto modulo 1: proymod1.
create database proymod1;

#Crear un usuario para nuestro proyecto modulo 1, dar acceso solo a esa tabla
# usuario y password: itladsgpo1
CREATE USER 'itladsgpo'@'localhost' IDENTIFIED BY '123456';

#Dar privilegios al usuario itladsgpo
GRANT ALL PRIVILEGES ON * . * TO 'itladsgpo'@'localhost';

#Segregar tabla de privilegios
FLUSH PRIVILEGES;

DROP USER 'itladsgpo'@'localhost';

#cambiar a base de datos creada
use proymod1;


#Cuando acceda nuevamente a Workbench hagalo con el usuario creado


#Borrar la base de datos
drop database proymod1;

select * from user