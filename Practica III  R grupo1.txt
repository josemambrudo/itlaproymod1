# Practica Grupal 
# Fecha: Sabado 11/02/2018
# Grupo 1
#
# Lider: Yaneris Betances
# Miembros: 
#   Rudy Inoa
#   Angel Ventura
#   Victor A. Sosa
#   Manuel Jose



# Tema 2 Objetos y operaciones basicas
y <- c(1,3,5,7) # c, funcion que genera un vector donde le indicamos los elementos.
y <- seq(1, 7, by=2)  # seq, genera un vector por secuencia, donde el inicio es 1, y final 7, incrementos o saltos de 2 

# Tema 3
x <- c(8,7,6,5)
y <- c(rep(3,8), rep(2,2)) # rep, genera repeticiones o series de datos, el primer argumento en el objeto a repterirse, mientras que el segundo las cantidades. Otra forma para generar esa seria seria, rep(c(3,2), c(8,2))
z <- seq(1,4, by=0.75) # seq, genera un vector donde sus elementos son una secuencia que inicia en 1 y termina en 4, donde el incremento es de 0.75


# Tema 4
chica <- c('Ana', 'Maria', 'Natalia', 'Almudena') # Genera un vector de cadena de textos, formados por nombre de chicas

# Tema 5
x <- c(2, -5, 4, 6, -2, 8) # Crea un vector con la funcion c, se asignan los valores a x
y <- x[x>0] # Filtramos los elementos de x, se obtienen los elementos positivos y asignamos a y
z <- x[x<0] # Filtramos los elementos de x, se obtienen los elementos negativos y asignamos a z
v <- x[2:6] # Filtramos los elementos de x, se obtienen los elementos negativos y asignamos a z
w <- x[seq(1,5, by =2)] # Filtramos o se han seleccionado los elemento en posicion impar del 1 al 5 

# Tema 6
x <- c(3, log(-15), 5) # Crear un vector, con los elementos indicados, en el caso del segundo elemento se produce un NAN o Not Avaible Number
x <- x[is.nan(x)==FALSE] # Filtramos los elementos de x, en este caso eliminamos 

# Tema 7
x <- c(1,2,3,4,5,6)
y <- c(7,8)
z <- c(9,10,11,12)
a <- x + x # 
b <- x + y # R iguala la longitud del menor vector a la del mayor reptiiendo la misma secuencia del menor vector
c <- x + z # R Completo la longitud del menor vector con los valores del mismo hasta llegar a la longitud del vector mayor

# Tema 8 Arrays y matrices
x <- seq(1,6) # crea un vector con la secuencia del 1 al 6, incremento por defecto de 1
m1 <- matrix(x, nrow=2) # Crea una matrix m1, con los elementos del vector del 1 a 6, formado por dos filas, divide la cantidad de elemento entre dos, resultando 3 columnas.
m2 <- matrix(x, ncol=2) # Crea una matrix m2, con los elementos del vector del 1 a 6, formado por dos columnas, divide la cantidad de elemento entre dos, resultando 3 filas
m3 <- matrix(x, nrow=2, byrow=TRUE) #Similar a m1, solo que al crear la matrix llena los elementos por fila, por defecto es por columna.
m4 <- matrix(x, nrow=3, ncol=3) # C

# Tema 9
# 1- Se crea la matriz con el numero de filas o columnas especificado y se va llenando esta hasta completar la cantidad de valores necesarios

# Tema 10
a <- matrix(c(2,1,3,4), nrow=2)
b <- matrix(c(3,8))
a * b # No se puede realizar porque las matrices son de tamano diferentes. Esta multiplicacion es elemento por elemento.
a %*% b # Multiplicacion de matrices. Se puede realizar porque a tiene tantas columnas cono b tiene filas
outer(a,b) #Realiza Outer Function. Es diferente a la multiplicacion matricial

# Tema 11 
# a) No se hace porque no tienen las misma dimensiones para multipicacion elemento por elemento
# b) Varias matrices de dimensiones 2x4
# c) Tipo matriz dimension 2x3
# d) Tipo matriz y dimensiones 2x4
# e) Tipo matriz y dimensiones 3x4
# f) matriz 2x3. Se puede realizar porque las matrices tienen las misma dimensiones
# g) no se puede realizar por que no cumplen con los requisitos para multiplicacion de matrices

# Tema 12 Listas, data framaes y factores
nombre <- c('Ana', 'Luis', 'Pedro', 'Juan', 'Eva', 'Jorge')
edad <- c(23, 24, 22, 24, 21, 22)
sexo <- c('M', 'H', 'H', 'H', 'M', 'H')
sexf <- factor(sexo, levels = c('M','H'))
summary(sexf)

# Tema 13
data.frame(nombre, edad, sexo) -> amigos
amigos

# Tema 14 
aggregate(amigos[,2],by=list(amigos$sexo),mean)
La edad media es para Hombres 23 y para Mujeres 22

# Tema 15 Graficos

x <- seq(0,2*pi, length=100)
plot(cos(x))
plot(x, cos(x))
# La diferencia es que en la primera grafica se omitieron los valores del eje X

# Tema 16

nominaARLSS2017 <- read.csv("http://www.arlss.gov.do/Generador_Documentos/Generador.aspx?format=csv&report=Nomin&anio=2017", sep = ",", header = TRUE  )
#read.csv, esta funcion lee un documento, en este caso la fuente es online obtenido de datos abierto dominicano, se le han indicado que los campos tienen separador, y posee una cabecera,
#se ha asignado a la variable nominaARLSS2017

summary(nominaARLSS2017) 
#La funcion summary, muestra un resumen de los datos del datafrme nominaARLSS2017

head(nominaARLSS2017)
#head, imprimit los primero 6 registros del dataframe nominaARLSS2017

plot(nominaARLSS2017$ESTATUS, nominaARLSS2017$SUELDO)
#plot, funcion que genera una grafica, donde x son los estados de los empleados y, y son los sueldos

